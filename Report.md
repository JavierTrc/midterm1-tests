# Los Estudiantes

## Descripción

Los estudiantes es una aplicación web que permite realzar la calificación de cursos y profesores en diferentes universidades. Está construida utilizando React para el frontend. No se tiene información sobre la arquitectura del back, debido a esto estamos limitados a pruebas de sistema sobre las funcionalidades expuestas en el sitio.

## Contexto

Solo se tiene 2.5 horas persona debido a restricciones de tiempo ya que es necesario realizar pruebas sobre otra aplicación. Con respecto a horas maquinas si se tiene más tiempo disponible. Se tiene hasta 5 horas máquinas para realizar pruebas. No se va a utilizar ningún servicio en la nube y la ejecución de las pruebas se realizará de forma local; en consecuencia, no es necesario alocar un presupuesto adicional para infraestructura.

## Objetivos

Debido a la mayor disponibilidad de tiempo maquina se enfocará en el uso de random testing para encontrar posibles errores en diferentes navegadores. Por otro lado, se probarán algunas de las funcionalidades consideradas más importantes utilizando BDD. De esta forma tener una cobertura mínima, pero tener la oportunidad de encontrar defectos.

## Reporte

La aplicación de los estudiantes no se le encontraron problemas al usar los gremlins para realizar random testing. Los escenarios validaron varias funcionalidades de las cuales ningún fallo. Uno de las más grandes faltas por tiempo fue realizar pruebas con distintos navegadores en diferentes resoluciones. En particular validar si los escenarios se cumplen también en móvil. Para los gremlins esto no hubiera sido mucho problema solo cambiando el tamaño del navegador al inicio de la prueba y en bdd se podría abstraer a un paso de la misma forma; estos son futuras mejoras que se pueden hacer al proceso.

# AntennaPod

## Descripción

AntennaPod es una aplicación open source para el manejo de podcasts. Esta permite encontrar podcasts utilizando servicios externos como itunes, fyyd y gpodder para luego suscribirse a ellos; una vez el usuario se ha suscrito a alguno puede descargar o escuchar a través streaming alguno de los episodios en el feed.  Debido a que esta es open source se puede hablar un poco más de la arquitectura.

## Arquitectura

La aplicación utiliza una librería core que tiene las funcionalidades principales mientras que la app como tal se encarga principalmente del manejo de las actividades y fragmentos. Para el manejo de guardado de los podcasts se utiliza un componente de storage el cual se encuentra en el core. Algunas solicitudes se realizan utilizando clientes http de bajo nivel, y se hacen aprovechando java RX para manejarlo como una tarea asíncrona.

## Contexto

Debido a las razones mencionadas en el contexto de la Los Estudiantes, Solo se tiene 2.5 horas persona. Se tiene hasta igualmente 5 horas máquinas para realizar pruebas. No se va a utilizar ningún servicio en la nube y la ejecución de las pruebas se realizará de forma local aprovechando algunas de las utilidades desarrolladas en el proyecto para lanzar distintos dispositivos con distintas versiones de Android.

## Objetivos

El enfoque en este punto también será random debido a la disponibilidad de horas maquinas. BDD se hará para cubrir algunos de los features más importantes. Esto alineado con los mismos objetivos generales planteados en los estudiantes.

## Reporte

Para realizar el diseño de los escenarios de BDD se realizaron algunas pruebas exploratorias sobre algunas funcionalidades. Debido al tiempo con el que se contó nos limitamos a escenarios básicos para agregar y eliminar podcasts. Durante la fase inicial donde se empezó a explorar las funcionalidades se encontró un bug el cual no fue posible reproducirlo. Estaba asociado a eliminar un podcast mientras se estaba reproduciendo alguno de sus episodios, se experimentó un comportamiento extraño en el cual no se pudo hacer nada y fue necesario cerrar la aplicación para retornar a un estado consistente.

Se corrieron los monkey tests pero ninguno fallo. Se utilizó una pequeña matriz de pruebas y sería necesario expandirla para tener mayor seguridad sobre la aplicación. Los escenarios de BDD estaban basados en las funcionalidades exploradas y también se corrieron sin problema