function loadScript(callback) {
  var s = document.createElement('script');
  s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
  if (s.addEventListener) {
    s.addEventListener('load', callback, false);
  } else if (s.readyState) {
    s.onreadystatechange = callback;
  }
  document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
  function isVisible( elem ) {
    return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
  }

  function stop() {
    horde.stop();
    callback();
  }
  var horde = window.gremlins.createHorde();
  horde.seed(9285434);

  //Sets the gremlins that make up the horde
  horde.gremlin(gremlins.species.formFiller().canFillElement(function(element) {
      var tag = element.tagName
      return isVisible(element) && (tag === 'INPUT' || tag === 'SELECT' || tag === 'TEXTAREA');
    }))
    .gremlin(gremlins.species.clicker().canClick(function(element) {
      var tag = element.tagName
      return isVisible(element) && (tag === 'A' || tag === 'BUTTON');
    }))
    .gremlin(gremlins.species.toucher().canTouch(function(element) {
      return isVisible(element)
    }))
    .gremlin(gremlins.species.scroller())
    .gremlin(gremlins.species.typer())

  //Setup the strategy for the horde 
  horde.strategy(gremlins.strategies.distribution().distribution([0.3, 0.4, 0.1, 0.1, 0.1]))

  horde.after(callback);
  window.onbeforeunload = stop;
  horde.unleash({ nb: 100000 });
}

describe('Monkey testing with gremlins ', function() {

  it('it should not raise any error', function() {
    browser.url('/');
    browser.click('button=Cerrar');

    browser.executeAsync(loadScript);

    browser.timeouts('script', 5*60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function() {
    browser.log('browser').value.forEach(function(log) {
      browser.logger.info(log.message.split(' ')[2]);
    });
  });

});