//Complete siguiendo las instrucciones del taller
var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When('I fill a wrong email and password', () => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys('wrongemail@example.com');

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys('123467891')
  });

  When('I try to login', () => {
    var cajaLogIn = browser.element('.cajaLogIn');
    cajaLogIn.element('button=Ingresar').click()
  });

  When(/^I fill with (.*) and (.*)$/ , (email, password) => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys(email);

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password)
   });

   When(/^I name myself as (.*) and (.*)$/, (firstName, lastName) => {
        var cajaSignUp = browser.element('.cajaSignUp');

        var nameInput = cajaSignUp.element('input[name="nombre"]');
        nameInput.click();
        nameInput.keys(firstName);

        var lastNameInput = cajaSignUp.element('input[name="apellido"]');
        lastNameInput.click();
        lastNameInput.keys(lastName);
   });

   When(/^I use as email: (.*)$/, email => {
        var cajaSignUp = browser.element('.cajaSignUp');

        var emailInput = cajaSignUp.element('input[name="correo"]');
        emailInput.click();
        emailInput.keys(email);
   });

   When(/^I use as password: (.*)$/, password => {
        var cajaSignUp = browser.element('.cajaSignUp');

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys(password);
    });

    When(/^I select as university: (.*)$/, university => {
        var cajaSignUp = browser.element('.cajaSignUp');

        var selectUni = cajaSignUp.element('select[name="idUniversidad"]');
        selectUni.selectByVisibleText(university)
    });

    When(/^I select as program: (.*)$/, program => {
        if(program) {
            var cajaSignUp = browser.element('.cajaSignUp');

            var selectProgram = cajaSignUp.element('select[name="idPrograma"]');
            selectProgram.selectByVisibleText(program)
        }
    });

    When(/^I write in the search bar: (.*)$/, search => {
      if(search) {
        browser.element(`div.Select-control input`).keys(search);
      }
    })

    When('I accept terms and conditions', () => {
        var cajaSignUp = browser.element('.cajaSignUp');

        var termsInput = cajaSignUp.element('input[name="acepta"]');
        termsInput.click();
    });

    When('I try to register', () => {
        var cajaSignUp = browser.element('.cajaSignUp');
        cajaSignUp.element('button=Registrarse').click();
    })

    Then(/^I want to see in the searchbar: (.*)$/, result => {
      if(result) {
        expect(browser.element(`*=${result}`)).to.not.be.an('undefined');
      }
    })

    Then('I expect to see {string}', error => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    var alertText = browser.element('.aviso.alert.alert-danger').getText();
    expect(alertText).to.include(error);
    });

    Then('I expect an alert with {string}', error => {
      browser.waitForVisible('.sweet-alert', 7000);
      var errorText = $('.sweet-alert').element('.lead').getText();
      expect(errorText).to.include(error);
      });

  Then('I expect to not be able to login', () => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
  });

  Then('I expect to be logged in', () => {
      browser.waitForVisible('#cuenta', 5000);
  })
});