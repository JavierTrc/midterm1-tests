Feature: Signin in losestudiantes
    As an user I want to be able to create an account in order to rate professors

Scenario Outline: Signing fails with wrong inputs

    Given I go to losestudiantes home screen
    When I open the login screen
    And I name myself as <firstName> and <lastName>
    And I use as email: <email>
    And I use as password: <password>
    And I select as university: <university>
    And I select as program: <program>
    And I accept terms and conditions
    And I try to register
    Then I expect to see <error>

    Examples:
    | firstName | lastName | email          | password   | university                     | program    | error                    |
    |           |          |                |            | Seleccionar una universidad... |            | Ingresa tu correo        |
    |           |          | invalidmail    |            | Seleccionar una universidad... |            | Ingresa un correo valido |
    |           |          | mail@gmail.com |            | Seleccionar una universidad... |            | Ingresa una contraseña   |
    | Javier    | Troconis | mail@gmail.com | contra     | Universidad Nacional           | Derecho    | La contraseña debe ser a |

Scenario: Signing failed without acceping terms and conditions

    Given I go to losestudiantes home screen
    When I open the login screen
    And I name myself as Javier and Troconis
    And I use as email: mail@gmail.com
    And I use as password: Contraseña
    And I select as university: Universidad Nacional
    And I select as program: Derecho
    And I try to register
    Then I expect to see Debes aceptar

Scenario: Signin failed because user already exists
    Given I go to losestudiantes home screen
    When I open the login screen
    And I name myself as Javier and Troconis
    And I use as email: javotrc@gmail.com
    And I use as password: Contraseña
    And I select as university: Universidad de los Andes
    And I select as program: Ingeniería de Sistemas y Computación
    And I accept terms and conditions
    And I try to register
    Then I expect an alert with Error: Ya existe un usuario registrado con el correo

# The following scenario is comented out because it only works the first time (and we didn't want to fill the db with fake users)
#Scenario: Signin succesfull
#    Given I go to losestudiantes home screen
#    When I open the login screen
#    And I name myself as Javier and Troconis
#    And I use as email: jav.troc28@gmail.com
#    And I use as password: Contraseña
#    And I select as university: Universidad de los Andes
#    And I select as program: Ingeniería de Sistemas y Computación
#    And I accept terms and conditions
#    And I try to register
#    Then I expect to see Debes aceptar
