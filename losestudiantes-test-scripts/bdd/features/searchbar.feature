Feature: Using the search bar
    As an user I want to be able to find teachers and subjects on the site

Scenario Outline: Searching teachers
    Given I go to losestudiantes home screen
    When I write in the search bar: <Input>
    Then I want to see in the searchbar: <Result>

    Examples:
    | Input           | Result                        |
    | Profesor Falso  | No se encontraron profesores  |
    | Mario Linares   | Mario Linares Vasquez         |

Scenario Outline: Searching subjects
    Given I go to losestudiantes home screen
    When I write in the search bar: <Input>
    Then I want to see in the searchbar: <Result>

    Examples:
    | Input           | Result                                   |
    | Materia falsa   | No se encontraron profesores ni materias |
    | Moviles         | Constr. Aplicaciones Móviles             |