const sh = require('shelljs');
var path = require('path');

const devices = ['Nexus 5X', 'pixel'];
const android_levels = [25, 26]
const test_matrix = [['Nexus_5x_25', 'pixel_25'], ['Nexus_5x_26', 'pixel_26']];
const avds = ['Nexus_5x_25', 'pixel_25', 'Nexus_5x_26', 'pixel_26']

const apkPath = path.normalize('../antennapod-play-debug.apk');
const packageName = 'de.danoeh.antennapod.debug';

const sdkmanager = path.normalize(`${process.env.ANDROID_HOME}/tools/bin/sdkmanager`);
const avdmanager = path.normalize(`${process.env.ANDROID_HOME}/tools/bin/avdmanager`);
const emulator = path.normalize(`${process.env.ANDROID_HOME}/emulator/emulator`);
const adb = path.normalize(`${process.env.ANDROID_HOME}/platform-tools/adb`);

const failureSeeds = [];

function createDevices() {
    android_levels.forEach( (level, i) => {
        const image = `system-images;${'android-' + level};google_apis;x86`
        sh.exec(`${sdkmanager} "${image}"`)
        devices.forEach((device, j) => {
            sh.exec(`${avdmanager} create avd -n "${test_matrix[i][j]}" -d "${device}" -k ${image} -f`)
        })
    })
}

let executeTests = async (deviceIndex) => {
    if(deviceIndex >= avds.length) return;
    const run = sh.exec(`${emulator} -avd ${avds[deviceIndex]} -memory 3072 -no-boot-anim -no-snapshot`, {async: true})
    // run.stdout.on('data', data => {
    //     console.log(data)
    // })
    let ready = false;
    await sleep(25000)
    sh.exec(`${adb} install -r -t ${apkPath}`)
    let seed = Math.floor(Math.random() * Math.floor(1000000000000))
    const monkey = sh.exec(`${adb} shell monkey -p ${packageName} -s ${seed} -v 1000`)
    if(monkey.code != 0) {
        failureSeeds.push(seed)
    }
    sh.exec(`${adb} emu kill`);
    return await executeTests(deviceIndex + 1)
}

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}

createDevices()
executeTests(0);