# Description
Tests run over the antennaPod app. The Apk was generated locally without the falttr api keys. (The integration is disabled). In order to test bdd you need to have an android emulator running, for the random part an android sdk is required in your machine.

## BDD
Was implemented using calabash steps. There is one additional custom step added to fulfill one of the actions we couldn't replicate easily with the provided steps.

### Running
You need to have installed ruby with bundle and run from the bdd folder the command:

`bundle exec calabash-run ../antennapod-play-debug.apk`

## Random
Using the monkey tester utility from android. Built using node (needs to be installed to run completely), inside the folder run the following 2 commands:

1. `npm install`
2. `node test.js`