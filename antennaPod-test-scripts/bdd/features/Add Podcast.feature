Feature: Add a podcast from itunes feature

  # Scenario: As an user I want to add a top podcast from itunes
  #   Given I press "Add Podcast"
  #   Then I press the "SEARCH ITUNES" button
  #   And I press "Dr. Death - Wondery"
  #   And I press the "SUBSCRIBE" button
  #   And I press the "OPEN PODCAST" button
  #   Then I wait
  #   And I drag from 0:10 to 100:0 moving with 10 steps
  #   Then I should see "6"

  # Scenario: An an user I want to search for a non existent podcast and not get results
  #   Then I wait for 2 seconds
  #   And I drag from 0:10 to 100:0 moving with 10 steps
  #   Given I press "Add Podcast"
  #   Then I press the "SEARCH ITUNES" button
  #   And I press "Search"
  #   And I enter "trashxcv" into input field number 1
  #   And I press the enter button
  #   Then I should see "No results were found"

  # Scenario: An an user I want to search for a podcast and add it
  #   Then I press the "SEARCH ITUNES" button
  #   And I press "Search"
  #   And I enter "pod save" into input field number 1
  #   And I press the enter button
  #   Then I press "Pod Save America"
  #   And I press the "SUBSCRIBE" button
  #   And I press the "OPEN PODCAST" button
  #   Then I wait
  #   And I drag from 0:10 to 100:0 moving with 10 steps
  #   Then I should see "194"