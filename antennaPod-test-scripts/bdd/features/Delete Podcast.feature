Feature: Delete a subscribed podcast

   Delete a podcast which the user previously subscribed to

    Scenario: Delete podcast from podcast screen
        Given I press "Add Podcast"
        Then I press the "SEARCH ITUNES" button
        And I press "Dr. Death - Wondery"
        And I press the "SUBSCRIBE" button
        And I press the "OPEN PODCAST" button
        And I wait for "Dr. Death" to appear
        And I press "More options"
        And I press "Remove podcast"
        And I press "Confirm"
        Then I should not see "Dr. Death"

    Scenario: Delete podcast from navigation drawer screen
        Then I wait for 2 seconds
        And I drag from 0:10 to 100:0 moving with 10 steps
        Given I press "Add Podcast"
        Then I press the "SEARCH ITUNES" button
        And I press "Dr. Death - Wondery"
        And I press the "SUBSCRIBE" button
        And I press the "OPEN PODCAST" button
        And I wait for "Dr. Death" to appear
        And I wait for 2 seconds
        And I drag from 0:10 to 100:0 moving with 10 steps
        And I wait
        # Have to avoid being in the same screen of the podcast
        And I press "Subscriptions"
        # Open the drawer again
        And I wait for 2 seconds
        And I drag from 0:10 to 100:0 moving with 10 steps
        # Delete the podcast
        Then I long press "Dr. Death"
        And I press "Remove podcast"
        And I press "Confirm"
        Then I should not see "Dr. Death"

    Scenario: Delete podcast from subscription screen
        Then I wait for 2 seconds
        And I drag from 0:10 to 100:0 moving with 10 steps
        Given I press "Add Podcast"
        Then I press the "SEARCH ITUNES" button
        And I press "Dr. Death - Wondery"
        And I press the "SUBSCRIBE" button
        And I press the "OPEN PODCAST" button
        And I wait for "Dr. Death" to appear
        And I wait for 2 seconds
        And I drag from 0:10 to 100:0 moving with 10 steps
        Then I press "Subscriptions"
        And I wait for 2 seconds
        And I long press first grid item
        And I press "Remove podcast"
        And I press "Confirm"
        And I wait
        And I drag from 0:10 to 100:0 moving with 10 steps
        Then I should not see "Dr. Death"