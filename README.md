# Description
For these tests you only need to have installed node. Both of the tests scripts work independently; regardless, both were made using webdriverio.

Additional info can be found on the resulting [report](Report.md)

## BDD
Using the cucumber framework for webdriver selenium standalone service. 

## Random
Suite made using gremlinjs with some modifications to the gremlins to reduce the rate of failed events. For now the seed needs to be set before each run (space for improvement)

### Running
You need to have installed node installed for both suites. Both suites can be run after executing the following commands:

1. `npm install`
2. `node test.js`